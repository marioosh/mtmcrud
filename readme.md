Example play framework application that demonstrate how works many-to-many relation with aditional property withing join column with crud module

how to run: 

- clone  


    $play deps  
    $play run
      
    go to [http://localhost:9999/admin](http://localhost:9999/admin)
      
[problem source](http://stackoverflow.com/questions/25908441/playframework-many-to-many-with-extra-columns-using-the-crud-module)