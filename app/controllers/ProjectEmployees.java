package controllers;

import models.ProjectEmployee;

/**
 * Created by Mariusz on 2014-09-19.
 */
@CRUD.For(ProjectEmployee.class)
public class ProjectEmployees extends CRUD {

    /**
     * CRUD show method doesn't know how to handle composite ids.
     *
     * @param id composite of ssn + "-" + accountId
     * @throws Exception
     */
    public static void show(String id) throws Exception {
        // Do not rename 'type' or 'object'
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        ProjectEmployee object = ProjectEmployee.findById(id);
        notFoundIfNull(object);
        render("CRUD/show.html", type, object);
    }


}
