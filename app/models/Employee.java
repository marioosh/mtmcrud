package models;


import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mariusz on 2014-09-19
 */
@Entity
public class Employee extends Model {
    public String name;

    @OneToMany(mappedBy = "employee")
    public List<ProjectEmployee> projectEmployees = new ArrayList<ProjectEmployee>();

    @Override
    public String toString() {
        return name;
    }
}
