package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;

/**
 * Created by Mariusz on 2014-09-19.
 */
@Entity
@Table(name = "project_employee")
public class ProjectEmployee extends GenericModel {


    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id", insertable = false, updatable = false)
    public Project project;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", insertable = false, updatable = false)
    public Employee employee;

    public boolean notified = false;

    public String _key() {
        try {
            return project.id + "-" + employee.id;
        } catch (NullPointerException npe) {
            return "0-0";
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("ProjectEmployee[");
        if (project != null) sb.append(project.name).append("-");
        if (employee != null) sb.append(employee.name);
        sb.append("]");
        return sb.toString();
    }

    public static ProjectEmployee findById(String id) {
        String[] elements = id.split("-");
        int projectId = Integer.valueOf(elements[0]);
        int employeeId = Integer.valueOf(elements[1]);

        return ProjectEmployee.find("project_id=? AND employee_id=?", projectId, employeeId).first();
    }
}
